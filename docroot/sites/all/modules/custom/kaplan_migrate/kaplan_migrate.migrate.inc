<?php

function kaplan_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'migrations' => array(
      'ItemNode' => array(
        'class_name' => 'ItemNodeMigration',
      ),
    ),
  );
  return $api;
}
