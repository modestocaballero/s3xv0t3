<?php

class ItemNodeMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Countries');

    $path = drupal_get_path('module', 'kaplan_migrate');
    $this->source = new MigrateSourceCSV(
      $path . '/csv/items.csv',
      array(),
      array(
        'header_rows' => 1,
      )
    );

    $this->destination = new MigrateDestinationNode('item');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'cid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Country ID.',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('body', 'description');

    $this->addFieldMapping('field_photos', 'image');
    $this->addFieldMapping('field_rating', 'rating');

    // Here we specify the directory containing the source files.
    $this->addFieldMapping('field_photos:source_dir')
      ->defaultValue($path . '/img');
  }
}

