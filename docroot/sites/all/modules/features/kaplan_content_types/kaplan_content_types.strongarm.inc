<?php
/**
 * @file
 * kaplan_content_types.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function kaplan_content_types_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cors_domains';
  $strongarm->value = array(
    '*' => 'http://www.kinder.fish',
  );
  $export['cors_domains'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'full_url_file_info_file_full_url';
  $strongarm->value = 1;
  $export['full_url_file_info_file_full_url'] = $strongarm;

  return $export;
}
