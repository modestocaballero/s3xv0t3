<?php
/**
 * @file
 * kaplan_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kaplan_content_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kaplan_content_types_node_info() {
  $items = array(
    'item' => array(
      'name' => t('Item'),
      'base' => 'node_content',
      'description' => t('Item to be voted.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
