<?php
/**
 * @file
 * kaplan_content_types.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function kaplan_content_types_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'bypass node access'.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
