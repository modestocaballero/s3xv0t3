/**
 * Created by Fabio on 18/08/15.
 */
'use strict';

var Action = React.createClass({
  render: function() {
    var class_name;
    if(this.props.icon == 'heart-o') {
      class_name = "fa fa-heart-o fa-2x";
    }
    else {
      class_name = "fa fa-times fa-2x"
    }
    return (
        <button id={this.props.id} onClick={this.props.callapi}>
            <i className={class_name}></i>
            <span>{this.props.text}</span>
        </button>
      );
  }
});

var User = React.createClass({
  getInitialState: function() {
    return {
        name: '',
        description: '',
        rate: 0,
        image: ''
    };
  },
  componentDidMount: function() {
    $.get(this.props.url, function(result) {
       if (this.isMounted()) {
           this.setState({
            name: result.goats[0].goat.fname,
            description: result.goats[0].goat.description,
            rate: result.goats[0].goat.rating,
            image: result.goats[0].goat.image.src 
          });
      }
    }.bind(this));
     
  },
  componentWillReceiveProps: function(nextProp) {
    $.get(nextProp.url, function(result) {
       if (this.isMounted()) {
          if(result.goats.length > 0) {
           this.setState({
            name: result.goats[0].goat.fname,
            description: result.goats[0].goat.description,
            rate: result.goats[0].goat.rating,
            image: result.goats[0].goat.image.src 
          });
        }
        else {
          nextProp.reset();
        } 
      }
    }.bind(this))
    .fail(function() {
      {nextProp.reset()}
    });
     
  },
  callApiYe:function() {
      {this.props.update()}
      $.ajax({
         url: 'http://api.kinder.fish/api/v1.0/node/'+this.props.user+'.json',
         type: 'PUT',
         data: {
          "field_rating": {
            "und": [
              {
                "value": parseInt(this.state.rate) + 1
              }
            ]
          }
         },
         success: function(response) {
         }
      });
    },
  callApiNo:function() {
     {this.props.update()}
     $.ajax({
         url: 'http://api.kinder.fish/api/v1.0/node/'+this.props.user+'.json',
         type: 'PUT',
         data: {
          "field_rating": {
            "und": [
              {
                "value": parseInt(this.state.rate) - 1
              }
            ]
          }
         },
         success: function(response) {
         }
      });
  },
  render: function() {
      var class_name,
      name=this.state.name,
      description=this.state.description,
      rate=this.state.rate,
      image=this.state.image,
      url = this.props.url;

      var divStyle = {
        backgroundImage: "url("+image+")"
      };

      if(this.props.icon == 'heart-o') {
        class_name = "fa fa-heart-o fa-3x like";
      }
      else {
        class_name = "fa fa-times fa-3x dislike"
      }

      return (
        <div id="user-wrapper">
             <div id="user">
                <figure id="user-image" style={divStyle}>         
                </figure>
                <div id="user-rating">
                  <div className="user-rating-value">{rate}</div>
                </div>
                <div id="user-choice">
                  <i className={class_name}></i>
                </div>
                <div id="user-info">
                    <div className="user-info-name">
                        <span className="user-info-fname">{name}</span>
                    </div>
                    <div className="user-description">
                        {description}
                    </div>
                </div>
            </div>
            <div id="app-rating-options">
             <Action id="like" text="YES" icon="heart-o" callapi={this.callApiYe}/>
              <Action id="dislike" text="NO" icon="times" callapi={this.callApiNo}/>
            </div>
          </div>
        );
  } 
});

var Goat = React.createClass({
    getInitialState: function() {
        return {
          initialIndex: 1,
          icon: 'heart-o'
        };
    }, 
    resetGoats: function() {
       this.setState({
          initialIndex: 1
        });
    },
    updateGoats: function() {
      this.setState({
          initialIndex: this.state.initialIndex + 1
        });
    },
    render: function() {  
      var api_url = "http://api.kinder.fish/api/v2.0/node.json/" + this.state.initialIndex;
          return (
            <div id="sexapp">
                <button id="menu">
                    <i className="fa fa-bars fa-4x"></i>
                </button>
              <User url={api_url} user={this.state.initialIndex} reset={this.resetGoats} update={this.updateGoats} />
            </div>
          );
    }
});

React.render(
<Goat />,
    document.getElementById('goat')
);